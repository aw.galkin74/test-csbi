USE [TaskService]
GO
/****** Object:  Table [dbo].[States]    Script Date: 04.05.2021 19:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[States](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StateName] [nvarchar](100) NOT NULL,
	[StateConst] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_States] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tasks]    Script Date: 04.05.2021 19:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tasks](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StateID] [int] NOT NULL,
	[Title] [nvarchar](250) NOT NULL,
	[DateFrom] [datetime] NOT NULL,
	[DateTo] [datetime] NOT NULL,
 CONSTRAINT [PK_Tasks] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[States] ON 

INSERT [dbo].[States] ([ID], [StateName], [StateConst]) VALUES (1, N'Решена', N'Resolved')
INSERT [dbo].[States] ([ID], [StateName], [StateConst]) VALUES (2, N'В процессе', N'InProccess')
INSERT [dbo].[States] ([ID], [StateName], [StateConst]) VALUES (3, N'Просрочена', N'
Expired')
SET IDENTITY_INSERT [dbo].[States] OFF
GO
SET IDENTITY_INSERT [dbo].[Tasks] ON 

INSERT [dbo].[Tasks] ([ID], [StateID], [Title], [DateFrom], [DateTo]) VALUES (1, 1, N'Задача №1', CAST(N'2021-05-04T00:00:00.000' AS DateTime), CAST(N'2021-05-04T00:00:00.000' AS DateTime))
INSERT [dbo].[Tasks] ([ID], [StateID], [Title], [DateFrom], [DateTo]) VALUES (3, 2, N'Задача №2', CAST(N'2021-05-03T00:00:00.000' AS DateTime), CAST(N'2021-05-03T00:00:00.000' AS DateTime))
INSERT [dbo].[Tasks] ([ID], [StateID], [Title], [DateFrom], [DateTo]) VALUES (4, 1, N'Задача №3', CAST(N'2021-05-04T18:00:00.000' AS DateTime), CAST(N'2021-05-04T19:00:00.000' AS DateTime))
INSERT [dbo].[Tasks] ([ID], [StateID], [Title], [DateFrom], [DateTo]) VALUES (5, 2, N'Задача №4', CAST(N'2021-05-04T19:00:00.000' AS DateTime), CAST(N'2021-05-04T21:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[Tasks] OFF
GO
/****** Object:  StoredProcedure [dbo].[GetStatesList]    Script Date: 04.05.2021 19:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Andrew Galkin
-- Create date: 04.05.2021
-- Description:	Getting task states list
-- =============================================

CREATE PROCEDURE [dbo].[GetStatesList] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT ST.ID, ST.StateName, ST.StateConst FROM [dbo].[States] AS ST

END
GO
/****** Object:  StoredProcedure [dbo].[GetTasksList]    Script Date: 04.05.2021 19:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrew Galkin
-- Create date: 04.05.2021
-- Description:	Getting task list by ConstName
-- =============================================
CREATE PROCEDURE [dbo].[GetTasksList] 
	@ConstName nvarchar(50)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TKS.ID, TKS.StateID, STS.StateName, TKS.Title, TKS.DateFrom, TKS.DateTo FROM [dbo].[Tasks] AS TKS

		LEFT JOIN [dbo].[States] AS STS
			ON STS.ID = TKS.StateID

	WHERE STS.StateConst IS NOT NULL AND (STS.StateConst = @ConstName OR @ConstName IS NULL)

END
GO
