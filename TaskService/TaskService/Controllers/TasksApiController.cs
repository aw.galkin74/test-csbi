﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskService.Abstractions;
using TaskService.Models;

namespace TaskService.Controllers
{
    [Route("task-api")]
    public class TasksApiController : Controller
    {
        private readonly ITaskService _taskService;

        public TasksApiController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        // GET: task-api/states
        [HttpGet("states")]
        public IActionResult GetTaskStates()
        {
            IEnumerable<StateViewModel> model = _taskService.ForEachTaskState();

            return new JsonResult(JsonConvert.SerializeObject(model));
        }

        // GET: task-api/tasks?stateConst={stateConst}
        [HttpGet("tasks")]
        public IActionResult GetAllTasks(string stateConst)
        {
            IEnumerable<TaskViewModel> model = _taskService.ForEachTask(stateConst);

            return new JsonResult(JsonConvert.SerializeObject(model));
        }
    }
}
