﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TaskService.Abstractions;
using TaskService.Models;

namespace TaskService.Services
{
    public class TasksService : ITaskService
    {
        private const string connectionString = "Server=localhost;Database=TaskService;MultipleActiveResultSets=true;Trusted_Connection=True;";
        public List<StateViewModel> ForEachTaskState()
        {
            SqlConnection connection = null;
            SqlDataReader reader = null;

            List<StateViewModel> result = new List<StateViewModel>();

            try
            {
                connection = new SqlConnection(connectionString);
                connection.Open();

                SqlCommand cmd = new SqlCommand("[dbo].[GetStatesList]", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        result.Add(new StateViewModel
                        {
                            ID = Int32.Parse(reader.GetValue(0).ToString()),
                            StateName = reader.GetValue(1).ToString(),
                            StateConst = reader.GetValue(2).ToString()
                        });
                    }
                }

            }
            finally
            {
                connection.Close();
                reader?.Close();
            }
            return result;
        }

        public List<TaskViewModel> ForEachTask(string stateConst)
        {
            SqlConnection connection = null;
            SqlDataReader reader = null;

            List<TaskViewModel> result = new List<TaskViewModel>();

            try
            {
                connection = new SqlConnection(connectionString);
                connection.Open();

                SqlCommand cmd = new SqlCommand("[dbo].[GetTasksList]", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@ConstName", stateConst));

                reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        TaskViewModel row = new TaskViewModel();

                        row.ID = Int32.Parse(reader.GetValue(0).ToString());
                        row.StateID = Int32.Parse(reader.GetValue(1).ToString());
                        row.StateName = reader.GetValue(2).ToString();
                        row.Title = reader.GetValue(3).ToString();
                        row.DateFrom = DateTime.Parse(reader.GetValue(4).ToString());
                        row.DateTo = DateTime.Parse(reader.GetValue(5).ToString());
                        row.Urgency = UrgencyCalculation(row.DateTo);
                        result.Add(row);
                    }
                }

            }
            finally
            {
                connection.Close();
                reader?.Close();
            }
            return result;
        }

        private bool UrgencyCalculation(DateTime dateTo)
        {
            DateTime dateNow = DateTime.Now;

            if (dateTo >= dateNow.AddHours(-2))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
