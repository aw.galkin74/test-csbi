﻿var app = angular
    .module('app', [])
    .factory('factoryTask', function ($http, $q) {
        var data = {};

        data.getStatesList = function () {
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: 'task-api/states'
            }).then(function success(response) {
                deferred.resolve(JSON.parse(response.data));
            }, function error(response) {
                deferred.reject(response.status);
            });
            return deferred.promise;
        }

        data.getTasksList = function (stateConst) {
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: 'task-api/tasks?stateConst=' + stateConst
            }).then(function success(response) {
                deferred.resolve(JSON.parse(response.data));
            }, function error(response) {
                deferred.reject(response.status);
            });
            return deferred.promise;
        }

        return data;
    });

app.controller('taskController', ['$scope', '$http', 'factoryTask', function ($scope, $http, factoryTask) {

    $scope.states = {};
    $scope.tasks = {};

    initStatesList();

    $scope.initTasksList = function(stateConst){
        var promiseObj = factoryTask.getTasksList(stateConst);

        promiseObj.then(function (response) {
            $scope.tasks = response;
            return $scope.tasks;
        });
    }

    function initStatesList() {
        var promiseObj = factoryTask.getStatesList();

        promiseObj.then(function (response) {
            $scope.states = response;
            return $scope.states;
        })
    }
}])