﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskService.Models;

namespace TaskService.Abstractions
{
    public interface ITaskService
    {
        List<StateViewModel> ForEachTaskState();

        List<TaskViewModel> ForEachTask(string stateConst);
    }
}
