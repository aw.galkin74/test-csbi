﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskService.Models
{
    public class TaskViewModel
    {
        public int ID { get; set; }

        public int StateID { get; set; }

        public string StateName { get; set; }

        public string Title { get; set; }

        public DateTime DateFrom { get; set; } = DateTime.Now;

        public DateTime DateTo { get; set; } = DateTime.Now;

        public bool Urgency { get; set; }
    }
}
