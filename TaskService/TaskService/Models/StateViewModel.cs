﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskService.Models
{
    public class StateViewModel
    {
        public int ID { get; set; }

        public string StateName { get; set; }

        public string StateConst { get; set; }
    }
}
